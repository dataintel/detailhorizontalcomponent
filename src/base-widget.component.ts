import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detailhorizontal',
  template: `<style>
  .img-news img{
    float:left;
    padding-right: 10px;
    height:250px;
    width:250px;
}

.article {
    -webkit-columns: 2 150px;
       -moz-columns: 2 150px;
            columns: 2 150px;
}

p {
  margin-bottom: 1.3em;
}

h1, h2, h3, h4 {
  margin: 1.414em 0 0.5em;
  font-weight: 700;
  line-height: 1.2;
}

h1 {
  margin-top: 0;
  font-size: 3.157em;
}

h2 {font-size: 2.369em;}

h3 {
  font-size: 1.777em;
  -webkit-column-span: all; 
  column-span: all; 
}

h4 {font-size: 1.333em;}

small, .font_small {font-size: 0.75em;}
  </style>
  <!--<div class="container">-->
    
        
            <!--<div class="row" style="padding: 10px; border: solid 2px rgba(224, 224, 224, 0.33); margin-right: -10px;">-->
                
                    
                <!--</div>-->
                
                    
                
            <!--</div>-->
        
    
<!--</div>-->
<div class="container" >
    <div class="col-md-10" style="padding: 10px; border: solid 2px rgba(224, 224, 224, 0.33); margin-right: -10px;" >
        <div class="col-xs-12" align="center" >
            <div class="article" align="center" *ngFor="let article of DetailComponent">
                 <h4 style="text-align:center">{{article.title}} </h4>
                <h5 style="font-size:12px; text-align:center">{{article.media_displayName}}, {{article.timestamp}}</h5>
                <img src="{{article.image}}" width="75%;" height="75%" style="margin-bottom: 10px;">
                <br><p align="justify">
                {{article.content}}
                
                </p>
                <!--</article>-->
            </div>
        </div>
    </div>
</div>
  
  `
})
export class BaseWidgetDetailHorizontalComponent implements OnInit {
  public DetailComponent: Array<any> = [
    {
        "title" : "Ketua Umum PBNU KH Said Aqil Siroj",
        "media_displayName" : "detik.com",
        "timestamp" : "2 days ago",
        "image" : "./src/assets/sample.jpg",
        "content" : " Conservative candidate Francois Fillon is holding on to the third place in opinion polls despite a scandal over payments of public funds to his family that has hurt his campaign. The 63-year-old former prime minister proposes a supply-side economic strategy with cuts in public spending, loosening restrictions on the length of the working week, and raising the retirement age. He is also a social conservative who wants to limit adoption rights of gay couples and has called for warmer ties with Russia. Here are Fillon's other key policy proposals: ECONOMY AND EMPLOYMENT - Ending the 35-hour week: Fillon proposes a return to a legal working week of 39 hours in the public and private sectors, up from the 35-hour week which since 2000 has obliged employers to pay higher rates or give time off for hours above the 35-hour mark. Fillon says the 39-hour week would apply straight away in the public sector and that negotiated deals in the private sector can allow people to work up to an EU ceiling of 48 hours. - Public sector jobs: He proposes cutting the headcount by 500,000 by not replacing all retiring civil servants and increasing the working week to 39 hours. Some five million - or one in five of the workforce - are employed in the civil service, local government and �"
    }    
];
  constructor() { }

  ngOnInit() {
  }

}
