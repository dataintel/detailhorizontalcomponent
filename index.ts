import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetDetailHorizontalComponent } from './src/base-widget.component';
export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
	BaseWidgetDetailHorizontalComponent
  ],
  exports: [
    BaseWidgetDetailHorizontalComponent
  ]
})
export class DetailHorizontalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DetailHorizontalModule,
      providers: []
    };
  }
}
